import { Component } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';
import { SharedServiceService } from './services/shared-service.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'poems';
  showFetch = true;
  showSpinner = false;
  constructor(private router: Router, private httpClient: HttpClient, private service: SharedServiceService) { }
  fetchPoems() {
    this.showFetch = false;
    this.showSpinner = true;
    this.poemService().subscribe(resp => {
      resp.forEach(element => {
        element.favorite = false;
      });
      this.service.setPoemList(resp);
      this.showSpinner = false;
      this.router.navigate(['/list']);
    });
  }
  poemService(): Observable<any> {
    return this.httpClient.get('https://poetrydb.org/random/20').pipe(map(data => data));
  }

}
