import { EventEmitter, Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SharedServiceService {
  listState = new BehaviorSubject('');
  selectedState = new BehaviorSubject('');
  faveListState = new BehaviorSubject('');
  setPoemList(list) {
    this.listState.next(list);
  }
  selectedPoem(msg) {
    this.selectedState.next(msg);
  }
}
