import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { SharedServiceService } from '../services/shared-service.service';

@Component({
  selector: 'app-poems-list',
  templateUrl: './poems-list.component.html',
  styleUrls: ['./poems-list.component.scss']
})
export class PoemsListComponent implements OnInit {
  poemList: any = [];

  constructor(private service: SharedServiceService, private router: Router) { }

  ngOnInit(): void {
    this.service.listState.subscribe(resp => this.poemList = resp);
  }
  nextPage(list) {
    this.service.selectedPoem(list);
    this.router.navigate(['/poems']);
  }
  setFavourites(index) {
    this.poemList[index].favorite = true;
    this.service.setPoemList(this.poemList);
  }
  removeFavourite(index) {
    this.poemList[index].favorite = false;
  }
}
