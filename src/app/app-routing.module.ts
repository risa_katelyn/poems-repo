import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PoemsListComponent } from './poems-list/poems-list.component';
import { PoemsComponent } from './poems/poems.component';

const routes: Routes = [
  {path: 'list', component: PoemsListComponent},
  {path: 'poems', component: PoemsComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
