import { Component, OnInit } from '@angular/core';
import { SharedServiceService } from '../services/shared-service.service';

@Component({
  selector: 'app-poems',
  templateUrl: './poems.component.html',
  styleUrls: ['./poems.component.scss']
})
export class PoemsComponent implements OnInit {
  poemList: any = [];
  selectedPoem: any = [];

  constructor(private service: SharedServiceService) { }

  ngOnInit(): void {
    this.service.selectedState.subscribe(data => this.selectedPoem = data);
    this.service.listState.subscribe(data => this.poemList = data);
  }
  setFavourite() {
    for (let i = 0; i < this.poemList.length; i++) {
      if (this.poemList[i].title == this.selectedPoem.title) {
        this.poemList[i].favorite = true;
      }
    }
  }
  removeFavourite() {
    for (let i = 0; i < this.poemList.length; i++) {
      if (this.poemList[i].title == this.selectedPoem.title) {
        this.poemList[i].favorite = false;
      }
    }
  }
}
